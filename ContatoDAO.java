package br.ucsal.bes20182.poo.atividade4.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.poo.atividade4.domain.Contato;

public class ContatoDAO {

	public List<Contato> contatos = new ArrayList<>();

	public String incluir(Contato contato) {
		contatos.add(contato);
		return null;
	}

	public List<Contato> obterTodos() {
		return contatos;
	}

}
